package main

import (
	"flag"
	"fmt"

	"gitlab.com/chloris_/tgms-parse-go/pkg/tgmsp"
)

// Program version
const version = "1.0.0"

// Date of release of this version
const date = "2020-10-08"

// File name for exported TG-MG data files
const exportedFileName = "tgms_export.asc"

// File name for exported 2D Gnuplot scripts
const gnuplot2dFileName = "plot_2d.gnuplot"

// File name for exported 3D Gnuplot scripts
const gnuplot3dFileName = "plot_3d.gnuplot"

func main() {
	// Parse command line arguments
	flagHelp := flag.Bool("h", false, "show help")
	flagVersion := flag.Bool("v", false, "show program version")
	flag.Parse()
	args := flag.Args()

	// Handle help and version flags, which return after printing the information
	if *flagHelp {
		fmt.Println("Usage:")
		flag.PrintDefaults()
		return
	}
	if *flagVersion {
		fmt.Printf("tgms-parse-go version %v (%v)\n", version, date)
		return
	}

	// Check for no or too many arguments
	if n := len(args); n == 0 {
		fmt.Println("No TG-MS data file path given")
		return
	} else if n > 1 {
		fmt.Printf("Too many arguments passed (%v). Only one file name argument is allowed.\n", n)
		return
	}

	// If everything is ok, do the main work
	// Parse the TG-MS data file
	path := args[0]
	parser := tgmsp.FileParser{}
	msData, err := parser.ParseFile(path)
	if err != nil {
		fmt.Printf("Error while parsing file '%v':\n%v\n", path, err)
		return
	}
	fmt.Printf("Data file '%v' parsed successfully\n", path)
	fmt.Printf("Number of parsed time points: %v\n", len(msData.TimePoints))

	// Export parsed data to a plottable format
	exporter := tgmsp.NewExporter(msData)
	err = exporter.Export(exportedFileName)
	if err != nil {
		fmt.Printf("Error while exporting file '%v':\n%v\n", exportedFileName, err)
		return
	}
	fmt.Printf("File '%v' succesfully exported\n", exportedFileName)

	// Write Gnuplot plot scripts
	err = exporter.WriteGnuplot2dPlotScript(gnuplot2dFileName)
	if err != nil {
		fmt.Printf("Error while writing 2D Gnuplot script:\n%v\n", err)
		return
	}
	fmt.Printf("Gnuplot 2D plot script '%v' written\n", gnuplot2dFileName)

	err = exporter.WriteGnuplot3dPlotScript(gnuplot3dFileName)
	if err != nil {
		fmt.Printf("Error while writing 3D Gnuplot script:\n%v\n", err)
		return
	}
	fmt.Printf("Gnuplot 3D plot script '%v' written\n", gnuplot3dFileName)
}
