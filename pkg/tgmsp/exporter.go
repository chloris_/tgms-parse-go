package tgmsp

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
)

// Exporter provides methods to export parsed data in a format better suited
// for plotting. It also provides methods for writing plot scripts.
// This struct should be constructed using NewExporter function.
type Exporter struct {
	msData           *MSData
	exportedFilePath string
}

// NewExporter creates a new instance of Exporter initialized with a pointer
// to the parsed data.
func NewExporter(msData *MSData) Exporter {
	return Exporter{msData: msData}
}

// Export exports parsed data to a file. Exported format is better suited for
// Plotting. The instance of Exporter remembers absolute path of the exported
// file for later use by generated plot scripts.
func (e *Exporter) Export(path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	for _, tp := range e.msData.TimePoints {
		line := ""
		// Print relative time
		line += fmt.Sprintf("%8.3f", tp.RelativeTime)
		// Print all (mass, ion current) data pairs
		for _, ic := range tp.Currents {
			line += fmt.Sprintf(" %6.2f %13.6e", ic.Mass, ic.IonCurrent)
		}
		// End line and write prepared line to file
		line += "\n"
		_, err := file.WriteString(line)
		if err != nil {
			return err
		}
	}

	// Remember the path of the exported file for use by plot scripts
	abs, err := filepath.Abs(path)
	if err != nil {
		return fmt.Errorf("Failed to get absolute path for file '%v'", path)
	}
	e.exportedFilePath = abs
	return nil
}

// WriteGnuplot2dPlotScript writes a 2D plot script for Gnuplot to the
// specified file. Data file must already be exported by this instance of
// Exporter using the method Export, since it remembers the path to the
// exported data file.
func (e *Exporter) WriteGnuplot2dPlotScript(path string) error {
	if e.exportedFilePath == "" {
		return errors.New("Data file has not been exported yet")
	}
	if len(e.msData.TimePoints) < 1 {
		return errors.New("No time points available")
	}

	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	// Write preamble
	line := "set xlabel \"Time [min]\"\n"
	line += "set ylabel \"Ion Current [A]\"\n\n"
	_, err = file.WriteString(line)
	if err != nil {
		return err
	}

	// Write instructions for each curve (each mass has a curve)
	for i, c := range e.msData.TimePoints[0].Currents {
		line := ""
		if i == 0 {
			line = fmt.Sprintf("plot \"%v\" u ", e.exportedFilePath)
		} else {
			line = fmt.Sprintf(",\\\n     \"%v\" u ", e.exportedFilePath)
		}

		dataColumn := 2*i + 3
		title := fmt.Sprintf("%.2f", c.Mass)
		line += fmt.Sprintf("1:%v w l title \"%v\"", dataColumn, title)

		_, err = file.WriteString(line)
		if err != nil {
			return err
		}
	}

	return nil
}

// WriteGnuplot3dPlotScript writes a 3D plot script for Gnuplot to the
// specified file. Data file must already be exported by this instance of
// Exporter using the method Export, since it remembers the path to the
// exported data file.
func (e *Exporter) WriteGnuplot3dPlotScript(path string) error {
	if e.exportedFilePath == "" {
		return errors.New("Data file has not been exported yet")
	}
	if len(e.msData.TimePoints) < 1 {
		return errors.New("No time points available")
	}

	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	// Write preamble
	line := "set ylabel \"Mass [amu]\" rotate parallel\n"
	line += "set xlabel \"Time [min]\" rotate parallel\n"
	line += "set zlabel \"Ion Current [A]\" rotate parallel offset -5\n\n"
	_, err = file.WriteString(line)
	if err != nil {
		return err
	}

	// Write instructions for each curve by iterating in reverse order.
	// Otherwise lines in the background of the fence plot would be visible
	// through lines in the foreground.
	tp := e.msData.TimePoints[0]
	lastI := len(tp.Currents) - 1
	for i := lastI; i >= 0; i-- {
		line := ""
		if i == lastI {
			line = fmt.Sprintf("splot \"%v\" u ", e.exportedFilePath)
		} else {
			line = fmt.Sprintf(",\\\n      \"%v\" u ", e.exportedFilePath)
		}

		icColumn := 2*i + 3
		massColumn := 2*i + 2
		title := fmt.Sprintf("%.2f", tp.Currents[i].Mass)
		// Columns: time (axis x), mass (axis y), z_base, z_base, ion current (axis z)
		line += fmt.Sprintf("1:%v:(0):(0):%v with zerrorfill title \"%v\"", massColumn, icColumn, title)

		_, err = file.WriteString(line)
		if err != nil {
			return err
		}
	}

	return nil
}
