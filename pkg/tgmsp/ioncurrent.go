package tgmsp

// IonCurrent represents a data pair of mass [amu] and ion current [A]
type IonCurrent struct {
	Mass       float64
	IonCurrent float64
}
