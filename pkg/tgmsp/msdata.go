package tgmsp

// MSData contains data parsed from a TG-MS data file.
type MSData struct {
	SourceFile string
	TimePoints []TimePoint
}
