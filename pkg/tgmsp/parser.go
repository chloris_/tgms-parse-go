package tgmsp

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// Shared regular expressions used in various parse stages
var regexpSourcefile = regexp.MustCompile(`^\s*Sourcefile\s+(.+)\s*$`)
var regexpStartTime = regexp.MustCompile(`^\s*Start Time\s+(.+)\s*$`)
var regexpTaskName = regexp.MustCompile(`^\s*Task\s+Name\s+Scan\s*$`)
var regexpDataHeader = regexp.MustCompile(`^\s*Mass\s+\[amu\]\s+Ion Current\s+\[A\]\s*$`)
var regexpDataLine = regexp.MustCompile(`^\s*(\S+)\s+(\S+)\s*$`)

// Layout to parse time from timestamps in TG-MS data files
const timeLayout = "1.2.2006 15:04:05.000"

// Stages of the parse process used by FileParser
type parseStage int

const (
	sourcefile parseStage = iota
	measurementStartTime
	taskName
	scanStartTime
	dataHeader
	dataLine
	endDataSeries
	abort
)

// Maximum number of non-empty lines to skip (if they don't contain
// desired data) before aborting the parse process
const maxSkippedLines = 10

// MaxFileSize is maximum size of TG-MS data file that FileParser will read.
// This is a safeguard against accidentally reading a huge file to memory.
const MaxFileSize = int64(10e6)

// FileParser parses a TG-MS data file and stores parsed data in a
// MSData object.
type FileParser struct {
	path                 string
	line                 string
	lineNumber           int
	skippedLines         int
	stage                parseStage
	err                  error
	msData               *MSData
	timePoint            *TimePoint
	measurementStartTime time.Time
	scanStartTime        time.Time
}

// ParseFile parses a TG-MS data file at provided path and returns parsed
// data.
func (p *FileParser) ParseFile(path string) (*MSData, error) {
	// Save the path and try to get file info
	p.path = path
	fileInfo, err := os.Stat(path)
	if err != nil {
		return nil, err
	}

	// Read the file only if it's not too large
	if size := fileInfo.Size(); size > MaxFileSize {
		return nil, fmt.Errorf("File '%v' exceeds maximum file size of %v bytes", path, size)
	}

	// Try to read the whole file to memory
	filebuf, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	// Reinitialize variables that require known initial value
	p.lineNumber = 0
	p.skippedLines = 0
	p.stage = sourcefile
	p.timePoint = nil

	// Read the buffered file line by line
	scanner := bufio.NewScanner(bytes.NewBuffer(filebuf))
	for scanner.Scan() {
		line := scanner.Text()
		p.lineNumber++

		// Skip empty lines, otherwise trim the line and save it to be shared
		// for use by parse stages
		if line == "" {
			continue
		}
		p.line = strings.TrimSpace(line)

		// Execute appropriate parse stage
		switch p.stage {
		case sourcefile:
			p.parseStageSourcefile()
		case measurementStartTime:
			p.parseStageMeasurementStartTime()
		case taskName:
			p.parseStageTaskName()
		case scanStartTime:
			p.parseStageScanStartTime()
		case dataHeader:
			p.parseStageDataHeader()
		case dataLine:
			p.parseStageDataLine()
		case endDataSeries:
			panic("Parse stage should never be 'endDataSeries' at this point")
		case abort:
			panic("Parse stage should never be 'abort' at this point")
		default:
			panic(fmt.Sprintf("Unknown parse stage %v", p.stage))
		}

		if p.stage == endDataSeries {
			p.parseStageEndDataSeries()
			// Run parse stage taskName before the next line is read
			p.parseStageTaskName()
		}

		if p.stage == abort {
			return nil, p.err
		}

		if p.skippedLines > maxSkippedLines {
			return nil, fmt.Errorf("More than %v lines contained no useful data", maxSkippedLines)
		}
	}

	// Append the last TimePoint if necessary
	if p.timePoint != nil {
		p.msData.TimePoints = append(p.msData.TimePoints, *p.timePoint)
	}

	// Return *MSData if it is populated
	if len(p.msData.TimePoints) > 0 {
		return p.msData, nil
	}
	// Return nil if there is no parsed data
	return nil, nil
}

// Executes parse stage sourcefile
func (p *FileParser) parseStageSourcefile() {
	matches := regexpSourcefile.FindStringSubmatch(p.line)
	if matches == nil {
		p.skippedLines++
	} else {
		p.skippedLines = 0
		p.msData = &MSData{SourceFile: matches[1]}
		p.stage = measurementStartTime
	}
}

// Executes parse stage measurementStartTime
func (p *FileParser) parseStageMeasurementStartTime() {
	matches := regexpStartTime.FindStringSubmatch(p.line)
	if matches == nil {
		p.skippedLines++
	} else {
		t, err := time.Parse(timeLayout, matches[1])
		if err != nil {
			p.stage = abort
			p.err = err
			return
		}

		p.measurementStartTime = t
		p.skippedLines = 0
		p.stage = taskName
	}
}

// Executes parse stage taskName
func (p *FileParser) parseStageTaskName() {
	if regexpTaskName.MatchString(p.line) {
		p.skippedLines = 0
		p.stage = scanStartTime

	} else {
		p.skippedLines++
	}
}

// Executes parse stage scanStartTime
func (p *FileParser) parseStageScanStartTime() {
	matches := regexpStartTime.FindStringSubmatch(p.line)
	if matches == nil {
		p.skippedLines++
	} else {
		t, err := time.Parse(timeLayout, matches[1])
		if err != nil {
			p.stage = abort
			p.err = err
			return
		}

		p.scanStartTime = t
		p.skippedLines = 0
		p.stage = dataHeader
	}
}

// Executes parse stage DataHeader
func (p *FileParser) parseStageDataHeader() {
	if regexpDataHeader.MatchString(p.line) {
		p.skippedLines = 0
		p.stage = dataLine

		// Prepare new TimePoint
		relativeTime := p.scanStartTime.Sub(p.measurementStartTime)
		p.timePoint = &TimePoint{RelativeTime: relativeTime.Minutes()}
	}
}

// Executes parse stage DataLine
func (p *FileParser) parseStageDataLine() {
	matches := regexpDataLine.FindStringSubmatch(p.line)
	if matches == nil {
		p.stage = endDataSeries
	} else {
		// Extract values and replace decimal commas with decimal points
		massString := strings.Replace(matches[1], ",", ".", 1)
		ionCurrentString := strings.Replace(matches[2], ",", ".", 1)

		// Convert string values to float, check for errors
		mass, err := strconv.ParseFloat(massString, 64)
		if err != nil {
			p.stage = abort
			p.err = err
			return
		}
		ionCurrent, err := strconv.ParseFloat(ionCurrentString, 64)
		if err != nil {
			p.stage = abort
			p.err = err
			return
		}

		// Package the data point and append it to the current time point
		ic := IonCurrent{
			Mass:       mass,
			IonCurrent: ionCurrent,
		}
		p.timePoint.Currents = append(p.timePoint.Currents, ic)
	}
}

// Executes parse stage endDataSeries
func (p *FileParser) parseStageEndDataSeries() {
	p.msData.TimePoints = append(p.msData.TimePoints, *p.timePoint)
	p.timePoint = nil
	p.stage = taskName
}
