package tgmsp

import (
	"testing"
)

// Tests ParseFile method of FileParser on a real shortened TG-MS data file.
func TestParseOnRealFile(t *testing.T) {
	parser := FileParser{}
	data, err := parser.ParseFile("../../test/short.asc")

	if err != nil {
		t.Fatalf("Error occured during parse process: %v", err)
	}

	if data == nil {
		t.Fatal("Parsed data is empty")
	}

	// Expected data
	sourcefileString := "Mia TG vz.142 p4K.qmp"
	masses := []float64{1.0, 2.0, 3.0}
	currents := [][]float64{
		[]float64{1.335948e-009, 6.376695e-011, 1.426444e-011},
		[]float64{1.342016e-009, 6.253027e-011, 1.360046e-011},
		[]float64{1.342463e-009, 6.241083e-011, 1.200700e-011},
	}
	times := []float64{0.0, 9.149 / 60.0, 18.293 / 60.0}

	if data.SourceFile != sourcefileString {
		t.Errorf("Expected sourcefile to be '%v', got '%v' instead", sourcefileString, data.SourceFile)
	}
	if n := len(data.TimePoints); n != 3 {
		t.Fatalf("Number of time points should be 3, is %v instead", n)
	}

	for i, tp := range data.TimePoints {
		if tp.RelativeTime != times[i] {
			t.Errorf("Timepoint [%v]: expected relative time %v, got %v", i, times[i], tp.RelativeTime)
		}
		for j, ic := range tp.Currents {
			if ic.Mass != masses[j] {
				t.Errorf("Timepoint [%v], data pair [%v]: expected mass %v, got %v", i, j, masses[j], ic.Mass)
			}
			if ic.IonCurrent != currents[i][j] {
				t.Errorf("Timepoint [%v], data pair [%v]: expected ion current %v, got %v", i, j, currents[i][j], ic.IonCurrent)
			}
		}
	}
}
