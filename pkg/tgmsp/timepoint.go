package tgmsp

// TimePoint represents a single time point in the TG-MS measurement.
// It contains time relative from the start of the measurement and collection
// of (mass, ion current) pairs at that time.
type TimePoint struct {
	RelativeTime float64
	Currents     []IonCurrent
}
